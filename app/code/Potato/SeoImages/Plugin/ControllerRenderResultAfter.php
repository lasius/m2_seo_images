<?php
namespace Potato\SeoImages\Plugin;

use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\Layout;
use Magento\Framework\App\Request\Http as HttpRequest;
use Potato\SeoImages\Model\Config;
use Potato\SeoImages\Model\Parser\Html as HtmlParser;
use Potato\SeoImages\Model\Optimisation\Processor;

class ControllerRenderResultAfter
{
    /** @var RequestInterface */
    protected $request;

    /** @var Config  */
    protected $config;

    /** @var HtmlParser  */
    protected $htmlParser;

    /** @var Processor  */
    protected $processor;

    /**
     * @param Config $config
     * @param RequestInterface $request
     * @param HtmlParser $htmlParser
     * @param Processor $processor
     */
    public function __construct(
        Config $config,
        RequestInterface $request,
        HtmlParser $htmlParser,
        Processor $processor
    ) {
        $this->config = $config;
        $this->request = $request;
        $this->htmlParser = $htmlParser;
        $this->processor = $processor;
    }

    /**
     * FPC will be called on afterRenderResult
     *
     * @param ResultInterface $subject
     * @param \Closure $proceed
     * @param ResponseHttp $response
     * @return Layout
     */
    public function aroundRenderResult(
        ResultInterface $subject,
        \Closure $proceed,
        ResponseHttp $response
    ) {
        $result = $proceed($response);
        if (!$this->htmlParser->isHtml($response->getBody())) {
            return $result;
        }
        if (!$this->config->isEnabled()) {
            return $result;
        }
        if (!($this->request instanceof HttpRequest)) {
            return $result;
        }
        $this->processor->processHtmlResponse($this->request, $response);
        return $result;
    }
}