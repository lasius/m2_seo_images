<?php
namespace Potato\SeoImages\Plugin;

use Magento\Catalog\Block\Product\View\Gallery as GalleryBlock;
use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Collection as DataCollection;
use Potato\SeoImages\Model\Config;
use Potato\SeoImages\Model\Optimisation\Processor\Url\Product as ProductUrlProcessor;

class ProductGallery
{
    /** @var Config  */
    protected $config;

    /** @var HttpRequest  */
    protected $request;

    /** @var ProductUrlProcessor  */
    protected $productUrlProcessor;

    /**
     * @param RequestInterface $request
     * @param Config $config
     * @param ProductUrlProcessor $productUrlProcessor
     */
    public function __construct(
        RequestInterface $request,
        Config $config,
        ProductUrlProcessor $productUrlProcessor
    ) {
        $this->request = $request;
        $this->config = $config;
        $this->productUrlProcessor = $productUrlProcessor;
    }

    /**
     * @param GalleryBlock $subject
     * @param \Closure $proceed
     *
     * @return DataCollection
     */
    public function aroundGetGalleryImages(
        GalleryBlock $subject,
        \Closure $proceed
    ) {
        if ($this->request->getModuleName() !== 'catalog' || $this->request->getControllerName() !== 'product') {
            return $proceed();
        }
        if (!$this->config->isPageUrlEnabled(Config::P_TYPE_PRODUCT)) {
            return $proceed();
        }
        $template = $this->config->getPageUrlTemplate(Config::P_TYPE_PRODUCT);
        /** @var DataCollection $result */
        $result = $proceed();
        foreach ($result as $item) {
            $urlList = [
                'small_image_url' => $item->getData('small_image_url'),
                'medium_image_url' => $item->getData('medium_image_url'),
                'large_image_url' => $item->getData('large_image_url'),
            ];
            foreach ($urlList as $key => $url) {
                $newUrl = null;
                try {
                    $newUrl = $this->productUrlProcessor->generateUrl($url, $template);
                } catch (\Exception $e) {
                    continue;
                }
                if (null === $newUrl) {
                    continue;
                }
                $item->setData($key, $newUrl);
            }
        }
        return $result;
    }
}