<?php
namespace Potato\SeoImages\Model\Optimisation\Processor;

use Magento\Framework\ObjectManagerInterface;
use Potato\SeoImages\Model\Config;

class AltFactory
{
    /** @var ObjectManagerInterface */
    protected $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string $type
     *
     * @return Alt\AltInterface;
     * @throws \Exception
     */
    public function create($type)
    {
        if ($type === Config::P_TYPE_PRODUCT) {
            return $this->objectManager->create(Alt\Product::class);
        }
        if ($type === Config::P_TYPE_CATEGORY) {
            return $this->objectManager->create(Alt\Category::class);
        }
        if ($type === Config::P_TYPE_CMS) {
            return $this->objectManager->create(Alt\Cms::class);
        }
        throw new \Exception('Unknown page type');
    }
}