<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\VariableCollector;

use Magento\Framework\View\LayoutInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\Page as CmsPage;

class Cms implements CollectorInterface
{
    /** @var LayoutInterface */
    protected $layout;

    /** @var PageFactory */
    protected $cmsPageFactory;

    /**
     * @param LayoutInterface $layout
     * @param PageFactory $cmsPageFactory
     */
    public function __construct(
        LayoutInterface $layout,
        PageFactory $cmsPageFactory
    ) {
        $this->layout = $layout;
        $this->cmsPageFactory = $cmsPageFactory;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getVariableList()
    {
        $identifier = null;
        foreach ($this->layout->getUpdate()->getHandles() as $handle) {
            if (strpos($handle, 'cms_index_index_id_') === 0) {
                $identifier = str_replace('cms_index_index_id_', '', $handle);
                break;
            }
        }
        if (null === $identifier) {
            throw new \Exception('Can not detect cms page');
        }
        /** @var CmsPage $cmsPage */
        $cmsPage = $this->cmsPageFactory->create();
        $cmsPage->load($identifier, 'identifier');
        if (null === $cmsPage->getId()) {
            throw new \Exception('Can not load cms page');
        }
        return [
            'page.name' => $cmsPage->getTitle(),
        ];
    }
}