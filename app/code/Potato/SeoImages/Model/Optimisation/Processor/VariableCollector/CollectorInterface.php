<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\VariableCollector;

interface CollectorInterface
{
    /**
     * @return array
     * @throws \Exception
     */
    public function getVariableList();
}