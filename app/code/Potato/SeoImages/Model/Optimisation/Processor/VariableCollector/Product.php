<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\VariableCollector;

use Magento\Framework\Registry;
use Magento\Catalog\Model\Product as ModelProduct;

class Product implements CollectorInterface
{
    /** @var Registry */
    protected $registry;

    /**
     * @param Registry $coreRegistry
     */
    public function __construct(
        Registry $coreRegistry
    ) {
        $this->registry = $coreRegistry;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getVariableList()
    {
        /** @var ModelProduct $product */
        $product = $this->registry->registry('current_product');
        if (null === $product) {
            throw new \Exception('Can not detect product');
        }
        
        $result = [
            'product.name' => $product->getName(),
            'product.sku' => $product->getSku()
        ];
        if ($product->getCategory()) {
            $result['product.category.name'] = $product->getCategory()->getName();
        }
        return $result;
    }
}