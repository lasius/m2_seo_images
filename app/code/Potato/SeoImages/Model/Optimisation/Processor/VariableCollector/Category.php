<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\VariableCollector;

use Magento\Framework\Registry;
use Magento\Catalog\Model\Category as ModelCategory;

class Category implements CollectorInterface
{
    /** @var Registry */
    protected $registry;

    /**
     * @param Registry $coreRegistry
     */
    public function __construct(
        Registry $coreRegistry
    ) {
        $this->registry = $coreRegistry;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getVariableList()
    {
        /** @var ModelCategory $category */
        $category = $this->registry->registry('current_category');
        if (null === $category) {
            throw new \Exception('Can not detect category');
        }
        return [
            'category.name' => $category->getName()
        ];
    }
}