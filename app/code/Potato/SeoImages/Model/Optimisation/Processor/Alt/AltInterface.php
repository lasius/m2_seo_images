<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\Alt;

interface AltInterface
{
    /**
     * @param string $html
     * @param string $template
     * @param bool $isCanChangeExist
     *
     * @return string
     * @throws \Exception
     */
    public function process($html, $template, $isCanChangeExist);
}