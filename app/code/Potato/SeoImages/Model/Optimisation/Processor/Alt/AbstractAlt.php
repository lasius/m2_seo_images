<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\Alt;

use Potato\SeoImages\Model\File\Manager as FileManager;
use Potato\SeoImages\Model\Optimisation\Processor\Finder\RegExp\Image as ImageFinder;
use Potato\SeoImages\Model\Optimisation\Processor\Finder\Result\Tag;
use Potato\SeoImages\Model\Parser\Html as HtmlParser;

abstract class AbstractAlt implements AltInterface
{
    /** @var ImageFinder */
    protected $imageFinder;

    /** @var HtmlParser */
    protected $htmlParser;

    /** @var FileManager */
    protected $fileManager;

    /**
     * @param ImageFinder $imageFinder
     * @param HtmlParser $htmlParser
     * @param FileManager $fileManager
     */
    public function __construct(
        ImageFinder $imageFinder,
        HtmlParser $htmlParser,
        FileManager $fileManager
    ) {
        $this->imageFinder = $imageFinder;
        $this->htmlParser = $htmlParser;
        $this->fileManager = $fileManager;
    }

    /**
     * @param string $html
     * @param string $template
     * @param bool $isCanChangeExist
     *
     * @return string
     * @throws \Exception
     */
    public function process($html, $template, $isCanChangeExist)
    {
        $increment = 1;
        $replaceData = [];
        $tagList = $this->imageFinder->findAll($html);
        foreach ($tagList as $tag) {
            /** @var Tag $tag */
            $attributeList = $tag->getAttributes();
            if (array_key_exists('alt', $attributeList) && !$isCanChangeExist) {
                continue;
            }
            if (array_key_exists('src', $attributeList) && !$this->isInternalMediaUrl($attributeList['src'])) {
                continue;
            }
            $replaceData[] = [
                'start'   => $tag->getStart(),
                'end'     => $tag->getEnd(),
                'content' => $tag->getContentWithUpdatedAttribute([
                    'alt' => $this->generateAlt($template, ['increment' => $increment])
                ])
            ];
            $increment++;
        }
        uasort($replaceData, [$this, 'sortByStartPos']);
        $replaceData = array_values($replaceData);
        foreach (array_reverse($replaceData) as $replaceElData) {
            $html = $this->htmlParser->replaceIntoHtml(
                $html, $replaceElData['content'], $replaceElData['start'], $replaceElData['end']
            );
        }
        return $html;
    }

    /**
     * @param string $template
     * @param array $additional
     *
     * @return string
     * @throws \Exception
     */
    public function generateAlt($template, $additional = array())
    {
        $result = $template;
        $variableList = array_merge($this->getVariableList(), $additional);
        foreach ($variableList as $key => $value) {
            $result = str_replace('{' . $key . '}', $value, $result);
        }
        return $result;
    }

    /**
     * @param string $url
     *
     * @return bool
     */
    protected function isInternalMediaUrl($url)
    {
        if (!$this->fileManager->isUrl($url)) {
            return false;
        }
        if (!$this->fileManager->isMediaUrl($url)) {
            return false;
        }
        return true;
    }

    /**
     * @param Tag $a
     * @param Tag $b
     *
     * @return int
     */
    protected function sortByStartPos($a, $b)
    {
        return $a['start'] - $b['start'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    abstract protected function getVariableList();
}