<?php
namespace Potato\SeoImages\Model\Optimisation\Processor;

use Magento\Framework\ObjectManagerInterface;
use Potato\SeoImages\Model\Config;

class UrlFactory
{
    /** @var ObjectManagerInterface */
    protected $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string $type
     *
     * @return Url\UrlInterface;
     * @throws \Exception
     */
    public function create($type)
    {
        if ($type === Config::P_TYPE_PRODUCT) {
            return $this->objectManager->create(Url\Product::class);
        }
        if ($type === Config::P_TYPE_CATEGORY) {
            return $this->objectManager->create(Url\Category::class);
        }
        if ($type === Config::P_TYPE_CMS) {
            return $this->objectManager->create(Url\Cms::class);
        }
        throw new \Exception('Unknown page type');
    }
}