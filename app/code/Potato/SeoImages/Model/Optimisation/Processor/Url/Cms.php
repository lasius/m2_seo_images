<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\Url;

use Potato\SeoImages\Model\File\Manager as FileManager;
use Potato\SeoImages\Model\Optimisation\Processor\Finder\RegExp\Image as ImageFinder;
use Potato\SeoImages\Model\Optimisation\Processor\VariableCollector\Cms as VariableCollector;
use Potato\SeoImages\Model\Parser\Html as HtmlParser;

class Cms extends AbstractUrl implements UrlInterface
{
    /** @var VariableCollector */
    protected $variableCollector;

    /**
     * @param ImageFinder $imageFinder
     * @param HtmlParser $htmlParser
     * @param FileManager $fileManager
     * @param VariableCollector $variableCollector
     */
    public function __construct(
        ImageFinder $imageFinder,
        HtmlParser $htmlParser,
        FileManager $fileManager,
        VariableCollector $variableCollector
    ) {
        parent::__construct($imageFinder, $htmlParser, $fileManager);
        $this->variableCollector = $variableCollector;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getVariableList()
    {
        return $this->variableCollector->getVariableList();
    }
}