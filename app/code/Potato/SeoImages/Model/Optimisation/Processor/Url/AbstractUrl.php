<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\Url;

use Potato\SeoImages\Model\Optimisation\Processor\Finder\RegExp\Image as ImageFinder;
use Potato\SeoImages\Model\Optimisation\Processor\Finder\Result\Tag;
use Potato\SeoImages\Model\Parser\Html as HtmlParser;
use Potato\SeoImages\Model\File\Manager as FileManager;

abstract class AbstractUrl implements UrlInterface
{
    const DIR_FILENAME_POSTFIX = '_poseo';

    /** @var ImageFinder */
    protected $imageFinder;

    /** @var HtmlParser */
    protected $htmlParser;

    /** @var FileManager */
    protected $fileManager;

    /**
     * @param ImageFinder $imageFinder
     * @param HtmlParser $htmlParser
     * @param FileManager $fileManager
     */
    public function __construct(
        ImageFinder $imageFinder,
        HtmlParser $htmlParser,
        FileManager $fileManager
    ) {
        $this->imageFinder = $imageFinder;
        $this->htmlParser = $htmlParser;
        $this->fileManager = $fileManager;
    }

    /**
     * @param string $html
     * @param string $template
     *
     * @return string
     * @throws \Exception
     */
    public function process($html, $template)
    {
        $replaceData = [];
        $tagList = $this->imageFinder->findAll($html);
        foreach ($tagList as $tag) {
            /** @var Tag $tag */
            $attributeList = $tag->getAttributes();
            if (!array_key_exists('src', $attributeList)) {
                continue;
            }
            $newUrl = $this->generateUrl($attributeList['src'], trim($template));
            if (null === $newUrl) {
                continue;
            }
            $replaceData[] = [
                'start'   => $tag->getStart(),
                'end'     => $tag->getEnd(),
                'content' => $tag->getContentWithUpdatedAttribute([
                    'src' => $newUrl
                ])
            ];
        }
        uasort($replaceData, [$this, 'sortByStartPos']);
        $replaceData = array_values($replaceData);
        foreach (array_reverse($replaceData) as $replaceElData) {
            $html = $this->htmlParser->replaceIntoHtml(
                $html, $replaceElData['content'], $replaceElData['start'], $replaceElData['end']
            );
        }
        return $html;
    }

    /**
     * @param string $src
     * @param string $template
     *
     * @return string|null
     * @throws \Exception
     */
    public function generateUrl($src, $template)
    {
        if (!$this->isInternalMediaUrl($src)) {
            return null;
        }
        $newFilename = $template;
        $variableList = array_merge($this->getVariableList());
        foreach ($variableList as $key => $value) {
            $newFilename = str_replace('{' . $key . '}', $value, $newFilename);
        }
        $newFilename = preg_replace('/\s+/', '-', strtolower($newFilename));
        $currentPath = $this->getPathFromSrc($src);
        if (null === $currentPath) {
            return null;
        }
        $targetPath = $this->generateTargetPath($currentPath, $newFilename);
        if (null === $targetPath) {
            return null;
        }
        return $this->convertPathToUrl($targetPath);
    }

    /**
     * @param Tag $a
     * @param Tag $b
     *
     * @return int
     */
    protected function sortByStartPos($a, $b)
    {
        return $a['start'] - $b['start'];
    }

    /**
     * @param string $src
     *
     * @return bool
     */
    protected function isInternalMediaUrl($src)
    {
        if (!$this->fileManager->isUrl($src)) {
            return false;
        }
        if (!$this->fileManager->isMediaUrl($src)) {
            return false;
        }
        return true;
    }

    /**
     * @param string $src
     *
     * @return string|null
     */
    protected function getPathFromSrc($src)
    {
        $path = $this->fileManager->convertUrlToPath($src);
        if (null === $path) {
            return null;
        }
        if (!file_exists($path)) {
            return null;
        }
        return $path;
    }

    /**
     * @param string $originPath
     * @param string $newFilename
     *
     * @return string|null
     */
    protected function generateTargetPath($originPath, $newFilename)
    {
        $newFilename .= '.' . pathinfo($originPath, PATHINFO_EXTENSION);
        $folderTry = 0;
        while ($folderTry < 100) {
            $targetPath = $originPath . self::DIR_FILENAME_POSTFIX
                . DIRECTORY_SEPARATOR . $folderTry . DIRECTORY_SEPARATOR . $newFilename;
            if (!file_exists($targetPath)) {
                $result = $this->fileManager->createSymlink($targetPath, $originPath);
                if (!$result) {
                    return null;
                }
                return $targetPath;
            }
            $linkPath = $this->fileManager->getSymlinkPointPath($targetPath);
            if ($linkPath === $originPath) {
                return $targetPath;
            }
            $folderTry++;
        }
        return null;
    }

    /**
     * @param string $targetPath
     *
     * @return string
     */
    protected function convertPathToUrl($targetPath)
    {
        return $this->fileManager->convertPathToUrl($targetPath);
    }

    /**
     * @return array
     * @throws \Exception
     */
    abstract protected function getVariableList();
}