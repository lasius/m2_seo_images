<?php
namespace Potato\SeoImages\Model\Optimisation\Processor\Url;

interface UrlInterface
{
    /**
     * @param string $html
     * @param string $template
     *
     * @return string
     * @throws \Exception
     */
    public function process($html, $template);
}