<?php
namespace Potato\SeoImages\Model\Optimisation;

use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\Framework\App\Response\Http as HttpResponse;
use Potato\SeoImages\Model\Config;
use Potato\SeoImages\Model\Optimisation\Processor\AltFactory;
use Potato\SeoImages\Model\Optimisation\Processor\UrlFactory;

class Processor
{
    /** @var Config  */
    protected $config;

    /** @var AltFactory  */
    protected $altFactory;

    /** @var UrlFactory  */
    protected $urlFactory;

    /**
     * @param Config $config
     * @param AltFactory $altFactory
     * @param UrlFactory $urlFactory
     */
    public function __construct(
        Config $config,
        AltFactory $altFactory,
        UrlFactory $urlFactory
    ) {
        $this->config = $config;
        $this->altFactory = $altFactory;
        $this->urlFactory = $urlFactory;
    }

    /**
     * @param HttpRequest $request
     * @param HttpResponse $response
     *
     * @return void
     */
    public function processHtmlResponse($request, $response)
    {
        $configPage = $this->getConfigPageTypeByRequest($request);
        if (!$this->config->isPageEnabled($configPage)) {
            return;
        }
        $resultHtml = $response->getBody();
        if ($this->config->isPageAltEnabled($configPage)) {
            $resultHtml = $this->processAltByPageType($configPage, $resultHtml);
            if (null === $resultHtml) {
                return;
            }
        }
        if ($this->config->isPageUrlEnabled($configPage)) {
            $resultHtml = $this->processUrlByPageType($configPage, $resultHtml);
            if (null === $resultHtml) {
                return;
            }
        }
        $response->setBody($resultHtml);
    }

    /**
     * @param string $configPage
     * @param string $html
     *
     * @return string|null
     */
    public function processAltByPageType($configPage, $html)
    {
        $template = $this->config->getPageAltTemplate($configPage);
        $isCanChangeExist = $this->config->isPageCanChangeExistsAlt($configPage);
        try {
            $altProcessor = $this->altFactory->create($configPage);
            $result = $altProcessor->process($html, $template, $isCanChangeExist);
        } catch (\Exception $e) {
            return null;
        }
        return $result;
    }

    /**
     * @param string $configPage
     * @param string $html
     *
     * @return string|null
     */
    public function processUrlByPageType($configPage, $html)
    {
        $template = $this->config->getPageUrlTemplate($configPage);
        try {
            $urlProcessor = $this->urlFactory->create($configPage);
            $result = $urlProcessor->process($html, $template);
        } catch (\Exception $e) {
            return null;
        }
        return $result;
    }

    /**
     * @param HttpRequest $request
     *
     * @return string
     */
    protected function getConfigPageTypeByRequest($request)
    {
        $moduleName = $request->getModuleName();
        if ($moduleName === 'cms') {
            return Config::P_TYPE_CMS;
        }
        if ($moduleName === 'catalog') {
            $controllerName = $request->getControllerName();
            if ($controllerName === 'category') {
                return Config::P_TYPE_CATEGORY;
            }
            if ($controllerName === 'product') {
                return Config::P_TYPE_PRODUCT;
            }
        }
        return '';
    }
}
