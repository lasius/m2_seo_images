<?php
namespace Potato\SeoImages\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;

class Config
{
    const P_TYPE_DEFAULT = '{page}';
    const P_TYPE_PRODUCT = 'product';
    const P_TYPE_CATEGORY = 'category';
    const P_TYPE_CMS = 'cms';

    const GENERAL_IS_ENABLED = 'potato_seo_images/general/is_enabled';

    const PAGE_IS_ENABLED = 'potato_seo_images/{page}/is_enabled';
    const PAGE_IS_ALT_ENABLED = 'potato_seo_images/{page}/is_enabled_alt';
    const PAGE_ALT_TEMPLATE = 'potato_seo_images/{page}/alt_template';
    const PAGE_ALT_IS_CHANGE_EXISTS = 'potato_seo_images/{page}/is_change_exists_alt';
    const PAGE_IS_URL_ENABLED = 'potato_seo_images/{page}/is_enabled_url';
    const PAGE_URL_TEMPLATE = 'potato_seo_images/{page}/url_template';

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::GENERAL_IS_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function isPageEnabled($page, $store = null)
    {
        return $this->isEnabled($store) && $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_IS_ENABLED, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function isPageAltEnabled($page, $store = null)
    {
        return $this->isPageEnabled($page, $store) && $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_IS_ALT_ENABLED, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function isPageUrlEnabled($page, $store = null)
    {
        return $this->isPageEnabled($page, $store) && $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_IS_URL_ENABLED, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return string
     */
    public function getPageAltTemplate($page, $store = null)
    {
        return $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_ALT_TEMPLATE, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function isPageCanChangeExistsAlt($page, $store = null)
    {
        return $this->isPageAltEnabled($page, $store) && $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_ALT_IS_CHANGE_EXISTS, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $page
     * @param null|string|bool|int|Store $store
     * @return string
     */
    public function getPageUrlTemplate($page, $store = null)
    {
        return $this->scopeConfig->getValue(
            $this->convertToPage(self::PAGE_URL_TEMPLATE, $page),
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param string $path
     * @param string $page
     *
     * @return string
     */
    protected function convertToPage($path, $page)
    {
        return str_replace(self::P_TYPE_DEFAULT, $page, $path);
    }
}