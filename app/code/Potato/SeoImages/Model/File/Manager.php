<?php
namespace Potato\SeoImages\Model\File;

use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class Manager
{
    /** @var StoreManagerInterface  */
    protected $storeManager;

    /** @var Filesystem  */
    protected $filesystem;

    /**
     * @param Filesystem $filesystem
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Filesystem $filesystem,
        StoreManagerInterface $storeManager
    ) {
        $this->filesystem = $filesystem;
        $this->storeManager = $storeManager;
    }

    /**
     * @param string $url
     *
     * @return string|null
     */
    public function convertUrlToPath($url)
    {
        try {
            $store = $this->storeManager->getStore();
        } catch (\Exception $e) {
            return null;
        }

        $originUrl = $url;
        foreach ($this->getBaseUrlPathList($store) as $baseUrlData) {
            if (strpos($url, $baseUrlData[0]) === 0) {
                $url = str_replace($baseUrlData[0], $baseUrlData[1], $url);
                break;
            }
        }
        if ($url === $originUrl) {
            return null;
        }
        if ($fragment = parse_url($url, PHP_URL_FRAGMENT)) {
            $url = str_replace('#' . $fragment, '', $url);
        }
        if ($query = parse_url($url, PHP_URL_QUERY)) {
            $url = str_replace('?' . $query, '', $url);
        }
        $url = rtrim($url, '?#');
        return $url;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function convertPathToUrl($path)
    {
        try {
            $store = $this->storeManager->getStore();
        } catch (\Exception $e) {
            return null;
        }
        $originPath = $path;
        foreach ($this->getBaseUrlPathList($store) as $baseUrlData) {
            if (strpos($path, $baseUrlData[1]) === 0) {
                $path = str_replace($baseUrlData[1], $baseUrlData[0], $path);
                break;
            }
        }
        if ($path === $originPath) {
            return null;
        }
        return $path;
    }

    /**
     * @param string $input
     *
     * @return bool
     */
    public function isUrl($input)
    {
        $isUrl = false;
        $isUrl = $isUrl || strpos($input, 'http://') === 0;
        $isUrl = $isUrl || strpos($input, 'https://') === 0;
        $isUrl = $isUrl || strpos($input, '//') === 0;
        return $isUrl;
    }

    /**
     * @param string $url
     *
     * @return bool
     */
    public function isMediaUrl($url)
    {
        try {
            $store = $this->storeManager->getStore();
        } catch (\Exception $e) {
            return false;
        }
        $baseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $secureBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA, true);
        return strpos($url, $baseUrl) === 0 || strpos($url, $secureBaseUrl) === 0;
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function isSymlink($path)
    {
        return is_link($path);
    }

    /**
     * @param $linkPath
     *
     * @return null|string
     */
    public function getSymlinkPointPath($linkPath)
    {
        if (!$this->isSymlink($linkPath)) {
            return null;
        }
        return readlink($linkPath);
    }

    /**
     * @param string $symlinkPath
     * @param string $symlinkToPath
     *
     * @return bool
     */
    public function createSymlink($symlinkPath, $symlinkToPath)
    {
        $path = str_replace(BP . DIRECTORY_SEPARATOR, '', $symlinkPath);
        $pathToTarget = BP;
        $pathMap = explode(DIRECTORY_SEPARATOR, $path);
        foreach ($pathMap as $key => $pathPart) {
            $pathToTarget .= DIRECTORY_SEPARATOR . $pathPart;
            if (file_exists($pathToTarget) && is_dir($pathToTarget)) {
                continue;
            }
            if ($key === (count($pathMap) - 1)) {//last element of array
                return symlink($symlinkToPath, $symlinkPath);
            }
            if (!@mkdir($pathToTarget)) {
                return false;
            }
        }
        return false;
    }

    /**
     * @param StoreInterface $store
     *
     * @return array
     */
    protected function getBaseUrlPathList(StoreInterface $store)
    {
        return [
            [
                $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA),
                $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
            ], [
                $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA, true),
                $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
            ], [
                $store->getBaseUrl(UrlInterface::URL_TYPE_STATIC),
                $this->filesystem->getDirectoryRead(DirectoryList::STATIC_VIEW)->getAbsolutePath()
            ], [
                $store->getBaseUrl(UrlInterface::URL_TYPE_STATIC, true),
                $this->filesystem->getDirectoryRead(DirectoryList::STATIC_VIEW)->getAbsolutePath()
            ], [
                $store->getBaseUrl(UrlInterface::URL_TYPE_WEB),
                $this->filesystem->getDirectoryRead(DirectoryList::ROOT)->getAbsolutePath()
            ], [
                $store->getBaseUrl(UrlInterface::URL_TYPE_WEB, true),
                $this->filesystem->getDirectoryRead(DirectoryList::ROOT)->getAbsolutePath()
            ],
        ];
    }
}